var Handlebars = require("handlebars");module.exports = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "<div>\n  <i class=\"fa fa-file-text fa-3x fa-pull-left\"></i>\n</div>\n<div>\n  <h1>"
    + escapeExpression(((helper = (helper = helpers.mainlabel || (depth0 != null ? depth0.mainlabel : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"mainlabel","hash":{},"data":data}) : helper)))
    + "</h1>\n  <h3>"
    + escapeExpression(((helper = (helper = helpers.sublabel || (depth0 != null ? depth0.sublabel : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"sublabel","hash":{},"data":data}) : helper)))
    + "</h3>\n</div>\n";
},"useData":true});