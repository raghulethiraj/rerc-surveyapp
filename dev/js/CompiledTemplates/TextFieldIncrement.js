var Handlebars = require("handlebars");module.exports = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression;
  return "<span>"
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.properties : depth0)) != null ? stack1.label : stack1), depth0))
    + "</span>\n<input type =\"number\" value =\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.properties : depth0)) != null ? stack1.value : stack1), depth0))
    + "\" step =\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.properties : depth0)) != null ? stack1.increment : stack1), depth0))
    + "\" title=\"Use up/down arrows to change value\">\n<span class=\"tooltip\">Use up/down arrows to change value</span>\n";
},"useData":true});