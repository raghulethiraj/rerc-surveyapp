var Handlebars = require("handlebars");module.exports = Handlebars.template({"1":function(depth0,helpers,partials,data) {
  var stack1, buffer = "  <p>\r\n    Tooltips for this input -\r\n";
  stack1 = helpers.each.call(depth0, ((stack1 = (depth0 != null ? depth0.properties : depth0)) != null ? stack1.tooltip : stack1), {"name":"each","hash":{},"fn":this.program(2, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer + "  </p>\r\n";
},"2":function(depth0,helpers,partials,data) {
  var helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression;
  return "      <span>"
    + escapeExpression(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"label","hash":{},"data":data}) : helper)))
    + "</span>,\r\n";
},"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, lambda=this.lambda, escapeExpression=this.escapeExpression, buffer = "<input type=\"number\" value=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.properties : depth0)) != null ? stack1.value : stack1), depth0))
    + "\" step=\""
    + escapeExpression(lambda(((stack1 = (depth0 != null ? depth0.properties : depth0)) != null ? stack1.increment : stack1), depth0))
    + "\">\r\n\r\n";
  stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 != null ? depth0.properties : depth0)) != null ? stack1.tooltip : stack1), {"name":"if","hash":{},"fn":this.program(1, data),"inverse":this.noop,"data":data});
  if (stack1 != null) { buffer += stack1; }
  return buffer;
},"useData":true});