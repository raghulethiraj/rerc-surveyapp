var Handlebars = require("handlebars");module.exports = Handlebars.template({"compiler":[6,">= 2.0.0-beta.1"],"main":function(depth0,helpers,partials,data) {
  var stack1, helper, functionType="function", helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, buffer = "<!-- Container for label and description -->\r\n<div class=\"question-group-title\">\r\n  <h3>"
    + escapeExpression(((helper = (helper = helpers.label || (depth0 != null ? depth0.label : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"label","hash":{},"data":data}) : helper)))
    + "</h3>\r\n  <p>";
  stack1 = ((helper = (helper = helpers.desp || (depth0 != null ? depth0.desp : depth0)) != null ? helper : helperMissing),(typeof helper === functionType ? helper.call(depth0, {"name":"desp","hash":{},"data":data}) : helper));
  if (stack1 != null) { buffer += stack1; }
  return buffer + "</p>\r\n  <hr />\r\n</div>\r\n\r\n<!-- Container for all the question -->\r\n<div class =\"questions-container\"></div>\r\n\r\n<div class=\"well red-well hidden\">\r\n  Please enter the required fields!\r\n</div>\r\n";
},"useData":true});