var Backbone  = require('backbone'),
    Questions = require('../Collections/Questions');

module.exports = Backbone.Model.extend({
  initialize: function(qGroup) {
    this.label     = qGroup.label;
    this.desp      = qGroup.desp;
    this.questions = new Questions(qGroup.questions);

    if (qGroup.customValidation) {
      this.validates = new Function(qGroup.customValidation);
    }
  },
  validates: function(attrs, options) {
    var res = true;

    this.questions.each(function(question) {
      if(!question.validates()) {
        res = false;
      }
    });

    if(!res) {
      this.trigger('showWarning');
    }

    return res ? true : false;
  }
});
