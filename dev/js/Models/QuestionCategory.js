var Backbone       = require('backbone'),
    QuestionGroups = require('../Collections/QuestionGroups');

module.exports = Backbone.Model.extend({
  initialize: function(qCategory) {
    this.questionGroups = new QuestionGroups(qCategory.questionGroups);

    if (qCategory.customValidation) {
      this.validates = new Function(qCategory.customValidation);
    }
  },
  validates: function() {
    var res = true;

    this.questionGroups.each(function(qGroup) {
      if(!qGroup.validates()) {
        res = false;
      }
    });

    return res ? true : false;
  }
});
