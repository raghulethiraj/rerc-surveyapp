var Backbone = require('backbone');
var Inputs   = require('../Collections/Inputs');

module.exports = Backbone.Model.extend({
  initialize: function(question) {
    this.label  = question.label;
    this.inputs = new Inputs(question.inputs);

    if (question.customValidation) {
      this.validates = new Function(question.customValidation);
    }
  },
  validates: function(attrs, options) {
    var res = true;

    this.inputs.each(function(input) {
      if(!input.isValid(true)) {
        res = false;
      }
    });

    return res ? true : false;
  }
});
