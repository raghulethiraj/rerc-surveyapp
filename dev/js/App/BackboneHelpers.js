var Backbone   = require('backbone'),
    BackboneValidation = require('backbone-validation'),
    _ = require('underscore');

Backbone.View.prototype.close = function(){
  if (this.onClose){
    this.onClose();
  }
  this.remove();
  this.unbind();
}

_.extend(Backbone.Model.prototype, Backbone.Validation.mixin);

module.exports = Backbone;
