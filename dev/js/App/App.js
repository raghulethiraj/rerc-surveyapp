// Libs
var Backbone        = require('backbone'),
    BackboneHelpers = require('./BackboneHelpers'),
    $               = require('jquery'),
    HBSHelpers      = require('./HBSHelpers')
    _               = require('underscore');

// Collections
var QuestionCategories   = require('../Collections/QuestionCategories');

// Views
var QuestionCategoryView = require('../Views/QuestionCategoryView'),
    SidenavView          = require('../Views/SidenavView');

// Templates
var HeaderTmpl = require('../CompiledTemplates/Header');

/*  This 'Class' is built with a selfdiator design pattern
    that uses event aggregators to control the work flow
*/
module.exports = function(props) {
  var self = this;

  self.url = props.url;

  self.$container     = $(props.container);
  self.$header        = self.$container.find('#survey-header');
  self.$sidenav       = self.$container.find('#side-nav');
  self.$progressBar   = self.$container.find('#progress-bar');
  self.$questionGroup = self.$container.find('#question-groups');

  self.$mainlogo    = $('#mainlogo');
  self.$partnerlogo = $('#partnerlogo');

  self.dispatcher     = _.extend({}, Backbone.Events);
  self.eventsSignaler = props.eventsSignaler;

  self.meta = props.meta;
  self.questionCategories = new QuestionCategories(props.qCategories, {url: props.url});

  // Starts the app
  self.turnup = function() {
    // Set up the dispatcher to listen for events
    self.dispatcher.listenTo(self.questionCategories, 'changeCategory', self.displayCategory);
    self.dispatcher.listenTo(self.questionCategories, 'loadNextCategory', self.nextCategory);
    self.dispatcher.listenTo(self.questionCategories, 'loadPrevCategory', self.prevCategory);

    self.render();
  };

  // Shutsdowns the app
  self.shutdown = function() {
    // Stop event listening
    self.dispatcher.stopListening();

    // Remove the page views
    if(self.curView) {
      self.curView.close();
    }
    if(self.sidenavView) {
      self.sidenavView.close();
    }
  };

  self.render = function() {
    // Render the header
    self.renderHeader();

    // Render the side nav
    self.renderSidenav();

    // Render the first category
    self.displayCategory(self.questionCategories.at(0));
  };

  self.renderHeader = function() {

    self.$mainlogo.attr('src', self.meta.mainlogo);
    self.$partnerlogo.attr('src', self.meta.partnerlogo);

    self.$header.empty();
    self.$header.html(HeaderTmpl(self.meta));
  };

  self.renderSidenav = function() {
    self.sidenavView = new SidenavView({collection: self.questionCategories});
    self.$sidenav.empty();
    self.$sidenav.html(self.sidenavView.render().el);
  };

  self.displayCategory = function(category) {
    // Close the current view before displaying a new one
    if(self.curView) {
      self.curView.close();
    }

    var index = self.questionCategories.indexOf(category);
    self.curView = new QuestionCategoryView({model: category});

    // Update the highlight of side nav to this category
    self.updateSidenav(index);

    // Display the category
    self.$questionGroup.append(self.curView.render().el).hide();

    self.$questionGroup.fadeIn();

    // Update/Hide button as needed
    if (index == 0) {
      self.$questionGroup.find('.back').hide();
    } else if (index == self.questionCategories.length - 1) {
      self.$questionGroup.find('.next').html('Submit');
    }

    // Update the progress bar to indicate progress
    var progressMade = ((index + 1) / (self.questionCategories.length)) * 100 ;
    self.$progressBar.find('progress').val(progressMade);

  };

  self.updateSidenav =  function(index) {
    // Remove the active class
    self.$sidenav.find('ul').children().removeClass('active');
    // Add active class to the right li eleselfnt
    self.$sidenav.find('ul').children().eq(index).addClass('active');
  }

  self.nextCategory = function(curCategory) {
    if (curCategory.validates()) {
      // Determine the next index
      var nextIndex = self.questionCategories.indexOf(curCategory) + 1;

      if(nextIndex < self.questionCategories.length) {
        self.displayCategory(self.questionCategories.at(nextIndex));
        $("html, body").animate({ scrollTop: 0 }, "slow");

        $.ajax({
          method: "POST",
          data: {
            Data: curCategory.toJSON()
          },
          url: curCategory.url()
        });
      } else if (nextIndex == self.questionCategories.length) {
        // If on the last category, POST all the categories
        // to the endpoint
        $.ajax({
          method: "POST",
          data: {
            Data: self.questionCategories.toJSON()
          },
          url: self.url
        }).done(function(res) {
          // trigger 'lastCategory' event
          self.eventsSignaler.trigger('lastCategory', res);
        }).fail(function(err) {
          console.log(err.responseJSON);
        });
      }
    }
  }

  self.prevCategory = function(curCategory) {
    if (curCategory.validates()) {
      var prevIndex = self.questionCategories.indexOf(curCategory) - 1;

      if(prevIndex >= 0) {
        self.displayCategory(self.questionCategories.at(prevIndex));
        $("html, body").animate({ scrollTop: 0 }, "slow");
      }
    }
  }
};
