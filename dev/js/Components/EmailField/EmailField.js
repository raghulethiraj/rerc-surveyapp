var TextField     = require('../TextField/TextField');

module.exports = TextField.extend({
  validation: {
    'properties.value': {
      required: function() {
        return (this.properties.properties.validate)?this.properties.properties.validate : false;
      },
      pattern: 'email',
      msg: 'Please enter a valid email'
    }
  }
});
