var Backbone     = require('backbone'),
    ImageTmpl = require('../../CompiledTemplates/Image.js');

module.exports = Backbone.View.extend({
  tagName: 'figure',
  template: ImageTmpl,
  initialize: function() {
    this.properties = this.model.get('properties');
  },
  render: function() {
    this.$el.html(this.template(this.properties));
    return this;
  }
});
