var Backbone     = require('backbone'),
    ImageView = require('./ImageView');

module.exports = Backbone.Model.extend({
  initialize: function(properties) {
    this.properties = properties;
    this.View       = ImageView;
  }
});
