var Backbone         = require('backbone'),
    SliderTmpl       = require('../../CompiledTemplates/Slider.js');

module.exports = Backbone.View.extend({
  tagName : 'div',
  template: SliderTmpl,
  className: 'slider',
  events  : {
    'mouseup input': 'updateValue',
    'change input':'updateVisual',
    'input input':'updateVisual'
  },
  initialize: function() {
    this.listenTo(this.model, 'change', this.render);
    this.properties = this.model.get('properties');
  },
  render: function() {
    this.$el.html(this.template(this.properties));
    this.$input = this.$('input');

    return this;
  },
  updateValue: function() {
    this.properties.value = this.$input.val();
    this.model.set(this.properties);
  },
  updateVisual: function() {
    if (this.$input.prop('min') != this.$input.val()) {
      this.$el.find('.display-value').html(this.$input.val());
    } else {
      this.$el.find('.display-value').empty();
    }
  }
});
