var Backbone   = require('backbone'),
    SliderView = require('./SliderView');

module.exports = Backbone.Model.extend({
  initialize: function(properties) {
    this.properties = properties;
    this.View       = SliderView;
  }
});
