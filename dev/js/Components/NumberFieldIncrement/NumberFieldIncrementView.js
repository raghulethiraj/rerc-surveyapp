var Backbone               = require('backbone'),
    NumberFieldIncrementTmpl = require('../../CompiledTemplates/NumberFieldIncrement.js');

module.exports = Backbone.View.extend({
  tagName : 'div',
  template: NumberFieldIncrementTmpl,
  className: "number-field-increment",
  events  : {
    'blur input': 'updateValue'
  },
  initialize: function() {
    this.listenTo(this.model, 'change', this.render);
    this.properties = this.model.get('properties');
  },
  render: function() {
    this.$el.html(this.template(this.properties));
    this.$input = this.$('input');

    return this;
  },
  updateValue: function() {
    var val = parseInt(this.$input.val());

  if (val < parseInt(this.$input.attr('min')) || val > parseInt(this.$input.attr('max'))) {
      this.$('.error-msg').removeClass('hidden');
      this.$el.addClass('has-error');
      this.$input.val('');
    } else {
      this.$('.error-msg').addClass('hidden');
      this.$el.removeClass('has-error');
    }
    this.properties.value = this.$input.val();
    this.model.set(this.properties);
  }
});
