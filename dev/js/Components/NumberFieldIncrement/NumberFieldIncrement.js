var Backbone                 = require('backbone'),
    NumberFieldIncrementView = require('./NumberFieldIncrementView');

module.exports = Backbone.Model.extend({
  initialize: function(properties) {
    this.properties = properties;
    this.View       = NumberFieldIncrementView;
  }
});
