var Radio            = require('../Radio/Radio'),
    LikertScaleView = require('./LikertScaleView');

module.exports = Radio.extend({
  initialize: function(properties) {
    this.properties = properties;
    this.View       = LikertScaleView;
  }
});
