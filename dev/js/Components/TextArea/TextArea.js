var Backbone     = require('backbone'),
    TextAreaView = require('./TextAreaView');

module.exports = Backbone.Model.extend({
  initialize: function(properties) {
    this.properties = properties;
    this.View       = TextAreaView;
  },
  validation: {
    'properties.value': {
      required: function() {
        if(this.properties.properties.validate) {
          return this.properties.properties.validate;
        } else {
          return false;
        }
      },
      msg: 'This is a required field'
    }
  }
});
