var Backbone     = require('backbone'),
    TextAreaTmpl = require('../../CompiledTemplates/TextArea.js');

module.exports = Backbone.View.extend({
  tagName  : 'div',
  className: 'text-area',
  template : TextAreaTmpl,
  events   : {
    "blur textarea": "updateValue"
  },
  initialize: function() {
    this.listenTo(this.model, "change", this.render);
    this.properties = this.model.get('properties');
  },
  render: function() {
    this.$el.html(this.template(this.properties));
    this.$input = this.$('textarea');

    // If this field is set to validate
    // add the required text
    if (this.properties.validate) {
      this.$('.error-msg').before('<span class="red">*Required</span>');
    }

    return this;
  },
  updateValue: function() {
    this.properties.value = this.$input.val();
    var errorMsg = this.model.preValidate('properties.value',this.properties.value);

    if (errorMsg === "") {
      this.model.set(this.properties);
      this.$('.error-msg').html('').addClass('hidden');
      this.$el.removeClass('has-error');
    } else if (typeof errorMsg == 'string') {
      this.$('.error-msg').html(errorMsg).removeClass('hidden');
      this.$el.addClass('has-error');
    }
  }
});
