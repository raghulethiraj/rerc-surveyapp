var Backbone     = require('backbone'),
    CheckboxView = require('./CheckboxView');

module.exports = Backbone.Model.extend({
  initialize: function(properties) {
    this.properties = properties;
    this.View       = CheckboxView;
  }
});
