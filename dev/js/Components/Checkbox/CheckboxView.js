var Backbone     = require('backbone'),
    CheckboxTmpl = require('../../CompiledTemplates/Checkbox.js');

module.exports = Backbone.View.extend({
  tagName : 'div',
  className: 'checkbox',
  template: CheckboxTmpl,
  events  : {
    'change input': 'updateValue'
  },
  initialize: function() {
    this.listenTo(this.model, 'change', this.render);
    this.properties = this.model.get('properties');
  },
  render: function() {
    this.$el.html(this.template(this.properties));
    this.$input = this.$('input');

    return this;
  },
  updateValue: function() {
    this.properties.checked = this.$input.prop('checked');
    this.model.set(this.properties);
  }
});
