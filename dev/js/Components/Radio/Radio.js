var Backbone  = require('backbone'),
    RadioView = require('./RadioView');

module.exports = Backbone.Model.extend({
  initialize: function(properties) {
    this.properties = properties;
    this.View       = RadioView;
  },
  validation: {
    'properties.value': {
      required: function() {
        return (this.properties.properties.validate)?this.properties.properties.validate : false;
      },
      msg: 'This is a required field'
    }
  }
});
