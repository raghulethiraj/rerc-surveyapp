var Backbone     = require('backbone'),
    RadioTmpl = require('../../CompiledTemplates/Radio.js');

module.exports = Backbone.View.extend({
  tagName : 'div',
  className: 'radio-input',
  template: RadioTmpl,
  events  : {
    'change input': 'updateValue'
  },
  initialize: function() {
    this.listenTo(this.model, 'change', this.render);
    this.properties = this.model.get('properties');
  },
  render: function() {
    this.$el.html(this.template(this.properties));

    // If this field is set to validate
    // add the required text
    if (this.properties.validate) {
      this.$('.error-msg').before('<span class="red">*Required</span>');
    }

    return this;
  },
  updateValue: function() {
    var selected = this.$('input[name="radio-group-'+this.properties.label+'"]:checked').val();
    this.properties.value = selected;
    this.model.set(this.properties);
  }
});
