var Backbone           = require('backbone'),
    TextFieldView      = require('./TextFieldView');

module.exports = Backbone.Model.extend({
  initialize: function(properties) {
    this.properties = properties;
    this.View       = TextFieldView;
  },
  validation: {
    'properties.value': {
      required: function() {
        return (this.properties.properties.validate)?this.properties.properties.validate : false;
      },
      msg: 'This is a required field'
    }
  }
});
