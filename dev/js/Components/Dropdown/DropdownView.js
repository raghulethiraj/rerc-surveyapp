var Backbone     = require('backbone'),
    DropdownTmpl = require('../../CompiledTemplates/Dropdown.js');

module.exports = Backbone.View.extend({
  tagName : 'div',
  template: DropdownTmpl,
  className: 'dropdown',
  events  : {
    'change select': 'updateValue'
  },
  initialize: function() {
    this.listenTo(this.model, 'change', this.render);
    this.properties = this.model.get('properties');
  },
  render: function() {
    this.$el.html(this.template(this.properties));
    this.$select = this.$('select');

    // If this field is set to validate
    // add the required text
    if (this.properties.validate) {
      this.$('.error-msg').before('<span class="red" style="display:block;">*Required</span>');
    }
    return this;
  },
  updateValue: function() {
    this.properties.value = this.$select.val();
    this.model.set(this.properties);
  }
});
