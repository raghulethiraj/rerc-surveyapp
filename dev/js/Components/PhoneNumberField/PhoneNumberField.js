var Backbone             = require('backbone'),
    PhoneNumberFieldView = require('./PhoneNumberFieldView');

module.exports = Backbone.Model.extend({
  initialize: function(properties) {
    this.properties = properties;
    this.View       = PhoneNumberFieldView;
  },
  validation: {
    'properties.value': {
      required: function() {
        return (this.properties.properties.validate)?this.properties.properties.validate : false;
      },
      length: 16,
      pattern: /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3} - [0-9]{4}$/,
      msg: 'Enter a valid 10 digit number'
    }
  }
});
