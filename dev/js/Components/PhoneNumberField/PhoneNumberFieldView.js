var TextFieldView = require('../TextField/TextFieldView');

module.exports = TextFieldView.extend({
  events   : {
    'blur input': 'updateValue',
    'input input': 'updateInput',
    'change input': 'updateInput'
  },
  updateInput: function() {
    var numbers = this.$input.val().replace(/\D/g, '').substr(0,10),
        char = {0:'(',3:') ',6:' - '};

    var phonenumer = '';

    for (var i = 0; i < numbers.length; i++) {
        phonenumer += (char[i]||'') + numbers[i];
    }

    this.$input.val(phonenumer);
  }
});
