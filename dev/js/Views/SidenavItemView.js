var Backbone        = require('backbone'),
    SidenavItemTmpl = require('../CompiledTemplates/SidenavItem');

module.exports = Backbone.View.extend({
  tagName : 'li',
  template: SidenavItemTmpl,
  events: {
    'click': 'changeCategory'
  },
  render: function() {
    this.$el.empty();
    this.$el.append(SidenavItemTmpl(this.model.attributes));

    return this;
  },
  changeCategory: function(event) {
    event.preventDefault();
    this.model.trigger('changeCategory', this.model);
  }
});
