var Backbone         = require('backbone'),
    QuestionViewTmpl = require('../CompiledTemplates/Question'),
    _                = require('underscore');

module.exports = Backbone.View.extend({
  tagName : 'div',
  className: 'question',
  template: QuestionViewTmpl,
  initialize: function() {
    this.views= [];
  },
  render  : function() {
    var self = this;
    // Empty the container
    this.$el.empty();

    // Render the template for this view
    this.$el.html(this.template(this.model.attributes));

    // Iterate through all the inputs in
    // this question and render it
    // using it's own render function
    this.model.inputs.each(function(input) {
      // Initialize each input's view
      var inputView = new input.View({model: input});
      self.views.push(inputView);

      // Render and append the input into the parent view
      this.append(inputView.render().el);

    }, this.$('.question-inputs')); // TODO: Might have to change the container

    return this;
  },
  onClose: function() {
    _.each(this.views, function(view) {
      view.close();
    });
  }
});
