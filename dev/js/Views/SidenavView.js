var Backbone        = require('backbone'),
    _               = require('underscore'),
    SidenavItemView = require('./SidenavItemView');

module.exports = Backbone.View.extend({
  tagName: 'ul',
  initialize: function() {
    this.views = [];
  },
  render: function() {
    var self = this;
    this.$el.empty();

    this.collection.each(function(qCategory) {
      var itemView = new SidenavItemView({model: qCategory});
      self.views.push(itemView);

      this.append(itemView.render().el);
    }, this.$el);

    return this;
  },
  onClose: function() {
    _.each(this.views, function(view) {
      view.close();
    });
  }
});
