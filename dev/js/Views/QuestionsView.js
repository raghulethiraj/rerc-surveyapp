var Backbone          = require('backbone'),
    QuestionsViewTmpl = require('../CompiledTemplates/Questions'),
    QuestionView      = require('./QuestionView'),
    _                 = require('underscore');

module.exports = Backbone.View.extend({
  tagName : 'div',
  template: QuestionsViewTmpl,
  initialize: function() {
    this.views = [];
  },
  render  : function() {
    var self = this;

    // Empty the container
    this.$el.empty();

    // Render the template for this view
    this.$el.html(this.template());

    // Iterate through all the questions in
    // this collection and render it
    // using it's own render function
    this.collection.each(function(question) {
      // Initialize each Question's view
      var questionView = new QuestionView({model: question});
      self.views.push(questionView);

      // Render and append the input into the parent view
      this.append(questionView.render().el);

    }, this.$el); // TODO: Might have to change the container

    return this;
  },
  onClose: function() {
    _.each(this.views, function(view) {
      view.close();
    });
  }
});
