var Backbone           = require('backbone'),
    _                  = require('underscore'),
    QuestionGroupView  = require('./QuestionGroupView'),
    QuestionGroupsTmpl = require('../CompiledTemplates/QuestionGroups');

module.exports = Backbone.View.extend({
  tagName : 'div',
  template: QuestionGroupsTmpl,
  initialize: function() {
    this.views = [];
  },
  render  : function() {
    var self =  this;
    this.$el.empty();
    this.$el.html(this.template());

    this.collection.each(function(qGroup) {
      var qGroupView = new QuestionGroupView({model: qGroup});
      self.views.push(qGroupView);

      this.append(qGroupView.render().el);

    }, this.$el);

    return this;
  },
  onClose: function() {
    _.each(this.views, function(view) {
      view.close();
    });
  }
});
