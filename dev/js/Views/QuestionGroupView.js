var Backbone          = require('backbone'),
    _                 = require('underscore'),
    QuestionGroupTmpl = require('../CompiledTemplates/QuestionGroup'),
    QuestionsView     = require('../Views/QuestionsView');

module.exports = Backbone.View.extend({
  tagName : 'div',
  className: 'question-group',
  template: QuestionGroupTmpl,
  initialize: function() {
    this.views = [];
    this.listenTo(this.model, 'showWarning', this.showWarning);
  },
  showWarning: function() {
    var self = this;
    this.$el.find('.well').fadeIn();
    setTimeout(function() {
      self.$el.find('.well').fadeOut();
    }, 3000);
  },
  render  : function() {
    this.$el.empty();

    this.$el.html(this.template(this.model.attributes));

    var questionsView = new QuestionsView({collection: this.model.questions});
    this.views.push(questionsView);

    this.$('.questions-container').append(questionsView.render().el);

    return this;
  },
  onClose: function() {    
    _.each(this.views, function(view) {
      view.close();
    });
  }
});
