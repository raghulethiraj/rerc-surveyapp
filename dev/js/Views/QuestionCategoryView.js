var Backbone             = require('backbone'),
    _                    = require('underscore'),
    QuestionGroupsView   = require('./QuestionGroupsView'),
    QuestionCategoryTmpl = require('../CompiledTemplates/QuestionCategory');

module.exports = Backbone.View.extend({
  tagName: 'div',
  template: QuestionCategoryTmpl,
  events: {
    'click .next': 'nextCategory',
    'click .back': 'prevCategory'
  },
  initialize: function() {
    this.views = [];
  },
  render : function() {
    this.$el.empty();

    this.$el.html(this.template());
    var qGroupsView = new QuestionGroupsView({collection: this.model.questionGroups});
    this.views.push(qGroupsView);

    this.$('.question-category').append(qGroupsView.render().el);

    return this;
  },
  nextCategory: function(event) {
    event.preventDefault();
    this.model.trigger('loadNextCategory', this.model);
  },
  prevCategory: function(event) {
    event.preventDefault();
    this.model.trigger('loadPrevCategory', this.model);
  },
  onClose: function() {
    _.each(this.views, function(view) {
      view.close();
    });
  }
});
