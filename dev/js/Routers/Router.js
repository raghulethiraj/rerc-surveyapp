var Backbone = require('backbone'),
    $        = require('jquery'),
    _        = require('underscore'),
    App      = require('../App/App');

var baseURL = "http://test.surveys.rerc.com/api/";

module.exports = Backbone.Router.extend({
  routes: {
    "survey/partner/:partner/:survey/user/general" : "loadUserSurvey",
    "survey/partner/:partner/user/:uid" : "loadUniqueUser",
    ""                                 : 'loadGenericUser'
  },
  loadApp: function(url) {
    var self = this;
    var eventsSignaler = _.extend({}, Backbone.Events);

    $.ajax({
      method: "GET",
      url: url
    }).done(function(res) {
      // Create the app for this resource
      var app = new App({
        container: 'main',
        url     : url,
        eventsSignaler: eventsSignaler,
        qCategories: res.Data.questionCategories,
        meta: res.Data.metadata
      });

      // Start the app
      app.turnup();

      // Listen when to shutdown the app
      eventsSignaler.once('lastCategory', function(res) {
        app.shutdown();

        var nextlink = _.find(res.Links, function(link) {
            return link.rel == 'nextlink';
        });

        if (nextlink) {
          // load the next link in the app
          self.facilitator(nextlink.link);
        } else {
          // redirect the user to some page
          console.log('redirecting user to some page');
        }
      });

    }).fail(function(error) {
      console.log(err.responseJSON);
    });
  },
  loadUniqueUser: function(partner,uid) {
    var genInfoURL = baseURL + "survey/partner/"+partner+"/user/"+uid+"/general";
    this.loadApp(genInfoURL);
  },
  loadGenericUser: function() {
    var genInfoURL = baseURL + "survey/user/general";
    this.loadApp(genInfoURL);
  },
  loadUserSurvey: function(partner, survey) {
    var url = baseURL + "survey/partner/"+ partner + "/" + survey + "/user/general/";
    this.loadApp(url);
  },
  facilitator :function(url) {
    this.loadApp(url);
  }
});
