var Backbone = require('backbone'),
    Question = require('../Models/Question');

module.exports = Backbone.Collection.extend({
  model: Question
});
