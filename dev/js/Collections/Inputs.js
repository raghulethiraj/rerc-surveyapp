var Backbone             = require('backbone'),
    TextArea             = require('../Components/TextArea/TextArea'),
    TextField            = require('../Components/TextField/TextField'),
    Checkbox             = require('../Components/Checkbox/Checkbox'),
    NumberFieldIncrement = require('../Components/NumberFieldIncrement/NumberFieldIncrement'),
    Slider               = require('../Components/Slider/Slider'),
    Radio                = require('../Components/Radio/Radio'),
    EmailField           = require('../Components/EmailField/EmailField'),
    PhoneNumberField     = require('../Components/PhoneNumberField/PhoneNumberField'),
    Image                = require('../Components/Image/Image'),
    LikertScale          = require('../Components/LikertScale/LikertScale'),
    Dropdown             = require('../Components/Dropdown/Dropdown');


module.exports = Backbone.Collection.extend({
  model: function(input, options) {
    switch (input.type) {
      case "text-area":
        return  new TextArea({properties: input.properties});
        break;
      case "text-field":
        return  new TextField({properties: input.properties});
        break;
      case "email-field":
        return  new EmailField({properties: input.properties});
        break;
      case "phone-number-field":
        return  new PhoneNumberField({properties: input.properties});
        break;
      case "dropdown":
        return new Dropdown({properties: input.properties});
        break;
      case "checkbox":
        return new Checkbox({properties: input.properties});
        break;
      case "number-field-increment":
        return  new NumberFieldIncrement({properties: input.properties});
        break;
      case "slider":
        return  new Slider({properties: input.properties});
        break;
      case "radio":
        return  new Radio({properties: input.properties});
        break;
      case "image":
        return  new Image({properties: input.properties});
        break;
      case "likert-scale":
        return  new LikertScale({properties: input.properties});
        break;
      default:
        this.type = null;
        console.error(input.type+" does not match any switch cases");
    }
  }
});
