var Backbone         = require('backbone'),
    QuestionCategory = require('../Models/QuestionCategory');

module.exports = Backbone.Collection.extend({
  model: QuestionCategory,
  initialize: function(models, options) {
    if (options && options.url) {
      this.url = options.url;
    }
  },
  parse: function(response) {
    return response.Data.questionCategories;
  }
});
