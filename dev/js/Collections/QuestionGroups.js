var Backbone      = require('backbone'),
    QuestionGroup = require('../Models/QuestionGroup');

module.exports = Backbone.Collection.extend({
  model: QuestionGroup
});
