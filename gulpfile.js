var gulp           = require('gulp'),
		uglify         = require('gulp-uglify'),
		eslint         = require('gulp-eslint'),
		browserify     = require('browserify'),
		source         = require('vinyl-source-stream'),
		buffer         = require('vinyl-buffer'),
		globby         = require('globby'),
		through        = require('through2'),
		sourcemaps     = require('gulp-sourcemaps'),
		gutil          = require('gulp-util'),
		ftp 					 = require('vinyl-ftp'),
		reactify       = require('reactify'),
		htmlmin        = require('gulp-htmlmin'),
		handlebars     = require('gulp-handlebars'),
		defineModule   = require('gulp-define-module'),
		browsersync    = require('browser-sync').create();

var stylus       = require('gulp-stylus'),
		nib          = require('nib'),
		axis         = require('axis'),
		postcss      = require('gulp-postcss'),
		lost         = require('lost'),
		rucksack     = require('rucksack-css'),
		autoprefixer = require('autoprefixer-core') ;

// Scripts Task
// Uglifies + Browserify
gulp.task('scripts', function() {
	//gulp.src('js/*.js')
	//	.pipe(uglify())
	//	.pipe(gulp.dest('build/js'));

	// set up the browserify instance on a task basis w/ 1 entry point
	var b = browserify({
		entries: 'dev/js/app.js',
		debug: true
	});

	return b.bundle()
				.pipe(source('main.js'))
				.pipe(buffer())
				.pipe(sourcemaps.init({loadMaps: true}))
				// Add transformation tasks to the pipeline here.
				//.pipe(uglify())
				.on('error', gutil.log)
				.pipe(sourcemaps.write('./'))
				.pipe(gulp.dest('build/js/'));
});

// Compiles Handlebar templates to be used w/ require
gulp.task('templates', function(){
  gulp.src(['dev/js/Templates/**/*.hbs'])
    .pipe(handlebars({
			handlebars: require('handlebars')
		}))
    .pipe(defineModule('node'))
    .pipe(gulp.dest('dev/js/CompiledTemplates/'));
});

// Lints the *.js files
gulp.task('lint', function() {
	return gulp.src(['dev/js/**/*.js'])
				.pipe(eslint())
				.pipe(eslint.format())
				.pipe(eslint.failAfterError());
});

gulp.task('minify-html', function() {
  return gulp.src('dev/*.html')
		    .pipe(htmlmin(
					{
						collapseWhitespace: true,
						conservativeCollapse: true
					}
				))
		    .pipe(gulp.dest('build/'))
});

// Styles Task
// Compiles styles
gulp.task('styles', function() {
	var processors = [
			lost,
			rucksack,
			autoprefixer
	];

	return gulp.src('dev/styles/styles.styl')
				.pipe(stylus({
					use: [nib(),axis()]
				}))
				.pipe(postcss(processors))
				.pipe(gulp.dest('build/css'))
				.pipe(browsersync.stream());
});

// Watch Task
// Watches file changes
gulp.task('watch', function() {
	gulp.watch('dev/js/Templates/**/*.hbs',['templates']);
	gulp.watch('dev/js/**/*.js', ['scripts']);
	gulp.watch('dev/**/*.html', ['minify-html']);
	gulp.watch('dev/styles/*.styl', ['styles']);

	gulp.watch('dev/js/CompiledTemplates/**/*.js').on('change', browsersync.reload);
	gulp.watch('build/*.html').on('change', browsersync.reload);
	gulp.watch('build/js/*.js').on('change', browsersync.reload);
});

// Browser auto sync
gulp.task('browser-sync', function() {
	browsersync.init({
		server: './build'
	});
});

// Push the code to server
gulp.task('deploy', function () {

    var conn = ftp.create( {
        host:     '72.3.218.68',
        user:     "rethiraj",
        password: '5itu5123!',
        parallel: 10,
        log:      gutil.log,
				secure: true,
				secureOptions: {rejectUnauthorized: false}
    } );

    var globs = [
        'build/js/**',
        'build/css/**',
        'build/index.html'
    ];

    // using base = '.' will transfer everything to /public_html correctly
    // turn off buffering in gulp.src for best performance

    return gulp.src( globs, { base: '.', buffer: false } )
        //.pipe( conn.newer( '/RERCTestserver/RERCNewSurveyApp/apitest' ) ) // only upload newer files
        .pipe( conn.dest( '/RERCTestserver/RERCNewSurveyApp/apitest' ) );

} );

// Gulp default tasks
gulp.task('default', ['minify-html','templates','scripts','styles','browser-sync','watch']);
